import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from './components/Main';
import { AppContextProvider } from './components/AppContext';
import Login from './components/Login';
import CreateRoom from './components/CreateRoom';
import MusicLayout from './components/MusicLayout';
import { Root } from "native-base";
import TrackHistoryList from './components/list/TrackHistoryList';
import TrackQueueList from './components/list/TrackQueueList';
import RoomUserList from './components/list/RoomUserList';

const Stack = createStackNavigator();

function App() {
    return (
        <AppContextProvider>
          <NavigationContainer screenOptions={{
              headerStyle: {
                backgroundColor: '#f4511e',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}>
            <Root>
              <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} options={({ title: 'Login' })}/>
                <Stack.Screen name="Main" component={Main} 
                  options={({ route }) => ({ title: 'Listen Together', headerLeft: null })}/>
                <Stack.Screen name="CreateRoom" component={CreateRoom} options={({ title: 'Create Room' })}/>
                <Stack.Screen name="MusicLayout" component={MusicLayout} 
                  options={({ route }) => ({ title: route.params.state.roomName})}/>
                <Stack.Screen name="TrackHistoryList" component={TrackHistoryList} options={({ title: 'Track History' })}/>
                <Stack.Screen name="TrackQueueList" component={TrackQueueList} options={({ title: 'Queue' })}/>
                <Stack.Screen name="RoomUserList" component={RoomUserList} options={({ title: 'Users' })}/>
              </Stack.Navigator>
            </Root>
          </NavigationContainer>
        </AppContextProvider>
      
    );
  }

  
  export default App;