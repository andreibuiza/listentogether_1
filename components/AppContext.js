import React, { Component } from 'react';
import db from '../data/Database.js';
import musicClient from './Spotify.js';
import Timer from 'react-native-timer';

const AppContext = React.createContext({
    playerState: {},
    updateUserState: (newState) => {},
    roomInfo: {},
    initializeRoomInfo: (newState) => {},
    subscribeToRoomTrack: (roomInfo) => {},
    playTrackOnSpotify: () =>{},
    pauseMusicPlayer: () =>{},
    playMusicPlayer: () => {},
    leaveRoom: () => {},
    state: {}
});

const defaultImageUri = 'https://reactnative.dev/img/tiny_logo.png';
const defaultTrackDetails = null;

class AppContextProvider extends Component {
    get remote() {
        return musicClient.remote;
    }

    state = {
        playerState : null,
        trackImageUri: defaultImageUri,
        userInfo : {
            isDj: false,
            display_name: "",
            uri: "",
        },
        roomInfo: {
            name: "",
            dj: "",
        }, 
        spotifyAuth: {
            token: null
        },
        trackHistory: [],   
        livePosition: 0,
        timerId: 0,
        trackQueue: [],
    }

    constructor(props) {
        super(props);
        this.updateUserState = this.updateUserState.bind(this)
        this.initializeRoomInfo = this.initializeRoomInfo.bind(this)
        this.authenticateWithSpotify = this.authenticateWithSpotify.bind(this)
        this.subscribeToRoomTrack = this.subscribeToRoomTrack.bind(this)
        this.playTrackOnSpotify = this.playTrackOnSpotify.bind(this)
        this.pauseMusicPlayer = this.pauseMusicPlayer.bind(this)
        this.playMusicPlayer = this.playMusicPlayer.bind(this)
        this.leaveRoom = this.leaveRoom.bind(this)
        this.clearTimeForMusicPosition = this.clearTimeForMusicPosition.bind(this)
    }

    componentDidMount() {
    }

    componentWillUnmount() {
        Timer.clearTimeout(this);
    }

    
    updateUserState(newUserState) {
        this.setState((state) => ({ ...state, userInfo: newUserState}));
    }

    clearTimeForMusicPosition = () => {
        if(this.state.timerId) {
            clearInterval(this.state.timerId);
        }
    }

    initializeLivePosition = () => {
        // start the timer
        let timerId = setInterval(() => {
            this.setState((state) => ({ ...state, livePosition: state.livePosition + 1000}));
        }, 1000);
        this.setState((state) => ({ ...state, timerId: timerId}));
    }

    initializeRoomInfo = (newRoomInfo) => {
        return db.getRoomInfo(roomInfoState => {
            console.log("initializing room info");
            this.setState((state) => ({ ...state, roomInfo: roomInfoState}))
            this.subscribeToRoomTrack(roomInfoState);

            db.subscribeToRoomInfo((modifiedRoomInfo) => {
                this.setState((state) => ({ ...state, roomInfo: modifiedRoomInfo}))
            }, this.state.roomInfo.name)

            db.subscribeToRoomTrackHistory((trackHistoryFromDb) => {
                this.setState((state) => ({ ...state, trackHistory: trackHistoryFromDb}))
            }, this.state.roomInfo.name)

            db.subscribeToRoomTrackQueue((queueItem) => {
                db.getQueue((queue) => {
                    if (queue) {
                        this.setState((state) => ({ ...state, trackQueue: queue}))
                    } else {
                        this.setState((state) => ({ ...state, trackQueue: []}))
                    }
                },
                this.state.roomInfo.name)
            }, this.state.roomInfo.name)

        }, newRoomInfo.name);
    }

    subscribeToRoomTrack = async (roomInfo) => {
        db.getCurrentSong(newState => {
            if (!newState) {
                this.setState((state) => ({ ...state, trackImageUri: defaultImageUri, playerState: defaultTrackDetails,
                livePosition: 0}))
                this.clearTimeForMusicPosition();
                return;
            }
            let playerState = newState

            this.setState((state) => ({ ...state,playerState: playerState}));
            this.setState((state) => ({ ...state, trackImageUri: playerState.image}));
            this.playTrackOnSpotify();
            
        }, roomInfo.name);
    }


    pauseMusicPlayer = () => {
        this.clearTimeForMusicPosition();
        let playerState = this.state.playerState;
        playerState.isPaused = true;
        this.setState((state) => ({ ...state, playerState: playerState}));
        musicClient.pauseMusic();
    }

    playMusicPlayer = () => {
        let playerState = this.state.playerState;
        if (!playerState) {
            return;
        }
        playerState.isPaused = false;
        this.setState((state) => ({ ...state, playerState: playerState}));
        this.playTrackOnSpotify();
    }

    playTrackOnSpotify = () => {
        let playerState = this.state.playerState;
        if (!playerState) {
            return;
        }
        console.log("syncing the time");
        this.clearTimeForMusicPosition();
        let currentTime = Date.now();
        let positionInTrack = currentTime - playerState.timeTrackStarted;
        if (positionInTrack > playerState.duration) {
            positionInTrack = 0;    
            playerState.isRoomTrackPaused = true;
            playerState.playbackPosition = 0;
        } else {
            playerState.isRoomTrackPaused = false;
            playerState.playbackPosition = positionInTrack;
        }

        this.setState((state) => ({ ...state, playerState: playerState, livePosition: positionInTrack}));
        if(this.state.playerState.isRoomTrackPaused || this.state.playerState.isPaused) {
            musicClient.pauseMusic();
        } else {
            this.initializeLivePosition()
            musicClient.playSong(playerState.uri, playerState.playbackPosition);
        }
    }
    /*
    TODO have a listener which would call pauseMusicPlayer if the user unsyncs from the room's track
    */

    leaveRoom() {
        this.remote.removeAllListeners();
        db.disconnectSong(this.state.roomInfo.name);
        db.disconnectRecommendation(this.state.roomInfo.name);
        db.disconnectRoomInfoSub(this.state.roomInfo.name);
        db.removeUserFromRoom(this.state.roomInfo.name, this.state.userInfo);    
        this.clearTimeForMusicPosition();
    }

    async authenticateWithSpotify() {
        try{
            const session = await musicClient.authenticate();
            this.setState((state) => ({ ...state, spotifyAuth: {token: session.accessToken}}));
            await musicClient.connectSpotifyRemote(this.state.spotifyAuth.token);
            let newUserInfo = await musicClient.getUserInfo();
            newUserInfo.isDj = false;
            this.setState((state) => ({ ...state, userInfo: newUserInfo}));
            console.log("User:")
            console.log(this.state.userInfo);
            return Promise.resolve(true);
        } catch(err) {
            console.log("Couldn't authorize with or connect to Spotify",err);
        }
        return Promise.resolve(false);
    }

    render() {
        const { children } = this.props
        return (
            <AppContext.Provider
                value={{
                    playerState : this.state.playerState,
                    updateUserState: this.updateUserState,
                    roomInfo: this.state.roomInfo,
                    initializeRoomInfo: this.initializeRoomInfo,
                    auth: this.auth,
                    authenticateWithSpotify: this.authenticateWithSpotify,
                    subscribeToRoomTrack: this.subscribeToRoomTrack,
                    playTrackOnSpotify: this.playTrackOnSpotify,
                    pauseMusicPlayer: this.pauseMusicPlayer,
                    playMusicPlayer: this.playMusicPlayer,
                    leaveRoom: this.leaveRoom,
                    state: this.state
                }}
            >
                {children}
            </AppContext.Provider>
        )
    }
}

export default AppContext;
export {AppContextProvider};