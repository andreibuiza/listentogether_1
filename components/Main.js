import React, { Component } from 'react';
import AppContext from './AppContext';
import { TextInput, StyleSheet } from 'react-native';
import {Text, Icon } from 'native-base';
import styles from './Styles.js';

import {
  View,
  Dialog,
  Colors,
  Button,
  PanningProvider,
  Constants,
  Toast
} from 'react-native-ui-lib';
import db from '../data/Database';
import RoomUserInfo from '../data/structure/RoomUserInfo.js';


class Main extends Component {
  state = { 
    showDialog: false,
    roomName: "",
    showTopToast: false,
    toastMessage: ""
  }
  context = this.context;

  dismissTopToast = () => {
    this.setState({showTopToast: false});
  }

  showTopToast = (message) => {
    this.setState({showTopToast: true, toastMessage: message});
  }

  getRoomCallback = async (existingRoomInfo) => {
    if(existingRoomInfo) {
      let updatedUserState = this.context.state.userInfo;
      updatedUserState.isDj = (existingRoomInfo.dj.id == this.context.state.userInfo.id)? true: false;
      this.context.updateUserState(updatedUserState)
      console.log("User:");
      console.log(updatedUserState);
      let basicUser = new RoomUserInfo(updatedUserState);
      db.addUserToRoom(existingRoomInfo.name, basicUser.get());
      await this.context.initializeRoomInfo(existingRoomInfo);
      this.hideDialog()
      // TODO be more specific on what you pass over
      this.props.navigation.navigate('MusicLayout', { state: this.state });
    } else {
      this.showTopToast("Room does not exist");
    }
};
  onPressJoin = () => {
    db.getRoomInfo(this.getRoomCallback, this.state.roomName)
  }

  onPressCreateNewRoom = () => {
    // TODO what is a better way to uniquely identify a room?
    // roomName = genUuid();s
    this.props.navigation.navigate('CreateRoom');
  }

  hideDialog = () => {
    this.setState({showDialog: false});
  };

  showDialog = () => {
    this.setState({showDialog: true});
  };
  onChangeRoomName = roomName => this.setState({ roomName });
  renderPlainContent = () => {
    return (
      <View>
        < Text style={styles.title}>Join room:</Text> 
        <TextInput
          onChangeText={this.onChangeRoomName}
          style={styles.nameInput}
          value={this.state.roomName}
        />
        <Button text60 label="Join" link onPress={this.onPressJoin}/>
      </View>
    );
  };

  renderDialog = () => {
    const {showDialog} = this.state;
    const height = '40%';

    return (
      <Dialog
        migrate
        useSafeArea
        height={height}
        panDirection={PanningProvider.Directions.UP}
        containerStyle={dialogStyle.roundedDialog}
        visible={showDialog}
        onDismiss={this.hideDialog}
        pannableHeaderProps={{title: 'This is a pannable header Dialog'}}
        supportedOrientations={['portrait', 'landscape']}
      >
        {this.renderPlainContent()}
      </Dialog>
    );
  };
  render() {
    return (
        <View>
          <Button large rounded iconLeft info
            style={styles.genericButton}
            onPress={this.showDialog}>
              <Icon name='people' />
              <Text>Join</Text>
          </Button>

          <Button large rounded iconLeft dark
            style={styles.genericButton}
            onPress={this.onPressCreateNewRoom}>
              <Icon name='add-circle' />
              <Text>Create</Text>
          </Button>
          {this.renderDialog()}
          <Toast
                visible={this.state.showTopToast}
                position={'top'}
                message={this.state.toastMessage}
                onDismiss={this.dismissTopToast}
                showDismiss={true}
                backgroundColor = {Colors.red30}
            />
        </View>
      );
  }
}

export const dialogStyle = StyleSheet.create({
  dialog: {
    backgroundColor: Colors.white
  },
  roundedDialog: {
    backgroundColor: Colors.white,
    marginBottom: Constants.isIphoneX ? 0 : 20,
    borderRadius: 12
  },
  button: {
    margin: 5,
    alignSelf: 'flex-start'
  },
  verticalScroll: {
    marginTop: 20
  },
  horizontalTextContainer: {
    alignSelf: 'center',
    position: 'absolute',
    top: 10
  }
});

Main.contextType = AppContext;
// TODO integrate in iOS native module for using Spotify SDK
// 

export default Main;
