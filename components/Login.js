import React, { Component } from 'react';
import styles from './Styles.js';
import AppContext from './AppContext';
import { View, Linking } from 'react-native';
import { Button, Text } from 'native-base';
import {
    Toast, Colors
  } from 'react-native-ui-lib';
class Login extends Component {
    context = this.context

    state = { 
        showTopToast: false,
        toastMessage: ""
      }

    dismissTopToast = () => {
        this.setState({showTopToast: false});
      }
    
    showTopToast = (message) => {
        this.setState({showTopToast: true, toastMessage: message});
    }

    
    onPressConnect = async () => {
        let spotifyUrl = "spotify:";
        const spotifyInstalled = await Linking.canOpenURL(spotifyUrl);
        if (spotifyInstalled) {
            this.context.authenticateWithSpotify().then(success => {
                if (success) {
                    if (this.context.state.userInfo.product == "premium") {
                        this.props.navigation.replace('Main');
                    } else {
                        this.showTopToast("This app requires Spotify Premium");
                    }
                } else {
                        this.showTopToast("Unable to connect to Spotify");
                }
            });
        } else {
            // TODO link the app to the platform specific app store
            this.showTopToast("Please install Spotify from the app store");
        }
        
    }

    render() {
        return (
          <View>
            <Text style={styles.title}>Listen Together</Text>  
            <Button large rounded success onPress={this.onPressConnect}
                style={styles.genericButton}><Text style={styles.buttonText}>Connect Spotify</Text></Button>
            <Text style={styles.header2}>Connect with Spotify to join a session and Listen Together!</Text> 
            <Toast
                visible={this.state.showTopToast}
                position={'top'}
                message={this.state.toastMessage}
                onDismiss={this.dismissTopToast}
                showDismiss={true}
                backgroundColor = {Colors.red30}
            /> 
          </View>);
      }
}
Login.contextType = AppContext;
export default Login;
