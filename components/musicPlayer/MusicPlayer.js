import React, { Component } from 'react';
import musicClient from '../Spotify.js';
import Slider from '@react-native-community/slider';

import { Duration } from 'luxon';
import { RepeatMode
} from 'react-native-spotify-remote';
import { Button, Switch, Segment, Icon } from 'native-base';
import AppContext from '../AppContext';
import styles from '../Styles';
import { TouchableOpacity, StyleSheet, View, Text, Image } from 'react-native';
import { Feather } from '@expo/vector-icons';
import ScoreController from './ScoreController';
import {
    Toast, Colors
} from 'react-native-ui-lib';
import {AppState} from "react-native";


class MusicPlayer extends Component {
    context = this.context
    
    state = { 
        showToast: null,
        toastMessage: "",
        appState: AppState.currentState

    }

    constructor() {
        super()
    }

    

  displayDuration(durationMs) {
    const d = Duration.fromMillis(durationMs);
    return d.hours > 0 ?
      d.toFormat("h:mm:ss")
      : d.toFormat("m:ss");
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {

    this.setState((state) => ({ ...state, appState: nextAppState}))

    // if (nextAppState === 'background') {
    //   // Do something here on app background.
    //   console.log("App is in Background Mode.")
    // }

    if (nextAppState === 'active') {
        this.joinRoomMusic();
    }
  };

  onError(e) {
      console.log("failure!");
      console.log(e);
  }

  isDj = () => {
    return this.context.state.userInfo.isDj;
    }

    joinRoomMusic = () => {
        this.setState((state) => ({ ...state, showToast: true, toastMessage: "Syncing Spotify to room's track"}));
        this.context.playMusicPlayer();
    }

    renderListenNow = () => {
        return (
            <TouchableOpacity
                onPress = {this.joinRoomMusic}
                >
                <View
                    style = {musicPlayerStyles.refresh}>
                    <Feather name="refresh-cw" size={40} color="black" />
                </View>
            </TouchableOpacity>
          );
    }

  render() {
    const buttons = [
        {
            content: <Icon name="md-skip-forward"/>,
            action: async () => {
                db.fire.skipSong(this.context.roomInfo.name);
            },
        }
    ];

    const updateCurrentMusicScore = (updatedScore) => {
        db.updateMusicScore(updatedScore, this.context.roomInfo.name);
    }

    return(
        <View style={styles.transportContainer}>
                <View style={{ display: "flex", alignItems: "center", justifyContent: "center", marginVertical: 10 }}>
                    <Image
                        style={musicPlayerStyles.trackImage}
                        source={{uri: this.trackImageUri}}
                    />
                    <Text style={{ fontSize: 20, fontWeight: "500" }}>{this.trackName}</Text>
                    {this.artistName !== "" && <Text style={{ fontSize: 17, fontWeight: "300" }}>{this.artistName}</Text>}
                </View>
                <ScoreController score={this.context.roomInfo.score} updateScore={updateCurrentMusicScore}/>
                <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                    <Text style={[styles.textDuration, styles.textCentered]}>{this.displayDuration(this.context.state.livePosition)}</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={this.duration}
                        value={this.context.state.livePosition}
                        disable={true}
                        style={{ flex: 1 }}
                    />
                    <Text style={[styles.textDuration, styles.textCentered]}>{this.displayDuration(this.duration)}</Text>
                </View>
                {this.isDj() ?
                    <>
                        <View>
                            <Segment>
                                {buttons.map(({ content, action }, index) => (
                                    <Button
                                        key={`${index}`}
                                        onPress={() => action().catch(this.onError)}
                                        transparent
                                        style={{
                                            width: `${(100 / buttons.length)}%`,
                                            height: 60,
                                            backgroundColor: "#FFF",
                                            borderLeftWidth: index === 0 ? 1 : 0,
                                            display:'flex',
                                            justifyContent:'center'
                                        }}
                                    >
                                        {content}
                                    </Button>
                                ))}
                            </Segment>
                        </View>
                    </>
                    :
                    <>
                    </>
                }
                {this.renderListenNow()}
                 {/* instead of roomInfo score, i need the actual track's score */}
                <Toast
                    visible={this.state.showToast}
                    position={'bottom'}
                    message={this.state.toastMessage}
                    onDismiss={() => this.setState((state) => ({ ...state, showToast: false}))}
                    showDismiss={false}
                    autoDismiss={1000}
                    backgroundColor = {Colors.green30}
                />

        </View>
    )
  }

    get remote() {
        return musicClient.remote;
    }

    get trackImageUri() {
        return this.context.state.trackImageUri;
    }
    get artistName() {
    return (this.context.playerState)? this.context.playerState.artists[0].name : "";
    }

    get duration() {
        return (this.context.playerState)? this.context.playerState.duration : 0;
    }

    get isRoomTrackPaused() {
        if (this.context.playerState) {
            if (this.context.playerState.isRoomTrackPaused || this.context.playerState.isPaused) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    get trackName() {
    return (this.context.playerState)? this.context.playerState.name : "";
    }
}
MusicPlayer.contextType = AppContext;

export default MusicPlayer;

const musicPlayerStyles = StyleSheet.create({
    refresh: {
        justifyContent: 'center',
        alignItems: 'center',
        display: "flex"
    },
    trackImage: {
        width: 240,
        height: 230,
    }
})
