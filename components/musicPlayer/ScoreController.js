import {View, Text} from 'react-native-ui-lib';
import {StyleSheet, TouchableOpacity} from 'react-native';
import { Feather } from '@expo/vector-icons'; 
import React, {useState} from 'react';

const styles = StyleSheet.create({
    score: {
      fontSize: 24, 
      color: "gray" 
    },
  });

export default function ScoreController({ score, updateScore }) {

    const onPress = () => {
        updateScore(score + 1);
    }

    return (
      <TouchableOpacity onPress={onPress}>
      <View marginB-100>
          <View centerH flex>  
              <Text style={styles.score}>{score}</Text>
              <Feather name="heart" size={32} color="black" />
          </View> 
      </View>
      </TouchableOpacity>
        
      );
}