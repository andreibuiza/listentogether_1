import { 
	auth as SpotifyAuth, 
	remote as SpotifyRemote, 
	ApiScope, 
	ApiConfig
} from 'react-native-spotify-remote';
import SpotifyWebApi from '../util/SpotifyWebApi';

const spotifyConfig = {
	clientID: "6d6884a12f95479ea32836d974ab7714",
	redirectURL: "spotifyloginsdklistentogether://",
	tokenRefreshURL: "http://891e06afedd1.ngrok.io/refresh",
	tokenSwapURL: "http://891e06afedd1.ngrok.io/swap",
	playURI: "",
	// TODO add more scopes here
	scopes: [ApiScope.AppRemoteControlScope, ApiScope.UserFollowReadScope
		, ApiScope.UserReadPrivateScope, ApiScope.UserReadEmailScope, ApiScope.UserLibraryModifyScope]
}

class Spotify {
	remote = SpotifyRemote;
	restClient = {};
	
	constructor() {
	}

	authenticate() {
		console.log("Authenticating with Spotify");
		return SpotifyAuth.authorize(spotifyConfig);
	}

	initialize() {
		console.log("Initializing  Spotify");
		return SpotifyAuth.initialize(spotifyConfig)
	}

	async connectSpotifyRemote(token) {
		console.log("connecting to spotify remote with token:" + token)
		await SpotifyRemote.connect(token);
		console.log("initializing Spotify rest client")
		this.restClient = new SpotifyWebApi(token)
	}

	/*
	Dj can SKIP current song and this should start the next song with a new start and end time.

	DJ must be able to add playlists
	DJ must be able to add whole albums (via search by album)

	TODO:
	 - DJ should be able to add to queue songs from History and Search results

	*/
	async playSong(songUri, playbackPosition){
		try{
			console.log("s: " + songUri + ", p: " + playbackPosition);
			await this.restClient.startSongAtPosition(songUri, playbackPosition);
		} catch(err){
			console.error("Couldn't play music",err);
		}
	}

	async queueSong(songUri){
		try{
			await SpotifyRemote.queueUri(songUri)
		} catch(err){
			console.error("Couldn't queue music",err);
		}
	}

	async pauseMusic(){
		try{
			await SpotifyRemote.pause()
		} catch(err){
			console.error("Couldn't pause music",err);
		}
	}

	sCallBack(session) {
		console.log("Authenticate successful");
		console.log(session);
		this.session = session;
	}

	eCallBack(name) {
		console.log(name + " error!");
	}

	getCurrentSong() {
		try {
			return SpotifyRemote.getPlayerState();
		} catch(err){
			console.error("Couldn't get music",err);
		}
	}

	getTrackInfo(id) {
		return this.restClient.getTrack(id);
	}

	getUserInfo() {
		return this.restClient.getUser();
	}

	addTrackToLikedSongs(id) {
		this.restClient.addTrackToYourMusic(id);
	}

	searchSpotifyEntity(q) {
		return this.restClient.searchSpotifyEntity(q);
	}

}
musicClient = new Spotify();
export default musicClient;