import React from 'react';
import DefaultItem from './DefaultItem';
import { TouchableOpacity} from "react-native";
import {spotifyIcon, heartIcon} from './Styles';
import {openInSpotify, deleteTrackFromQueue} from './DefaultItemMethods';

export default function TrackQueueItemAdmin({ queueItem, index, drag, isActive, context }) {
    
    const options = [
            { label: 'Open on Spotify', onPress: () => openInSpotify(queueItem.album.uri), icon: spotifyIcon },
            { label: 'Add to Liked Songs', onPress: () => spotifyLike(queueItem.id), icon: heartIcon}, 
            { label: 'Delete from Queue', onPress: () => deleteTrackFromQueue(index, context), icon: heartIcon}, ];
    return (
        <TouchableOpacity onLongPress={drag}>
            <DefaultItem item={queueItem} options={options}/> 
        </TouchableOpacity>
      );
}
