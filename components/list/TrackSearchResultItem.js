import styles, {spotifyIcon, heartIcon, recommendIcon} from './Styles';
import React, {useState}from 'react';
import { TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import {View, Text, ActionSheet, Card, Colors, Image,  Dialog, PanningProvider} from 'react-native-ui-lib';
import {openInSpotify, spotifyLike, recommend, PreviewTrack, queueTrackFromSearch} from './DefaultItemMethods';
import {dialogStyle} from '../Main';


export default function TrackSearchResultItem({ trackInfo, context, isDj }) {
    const [showOptions, setShowOptions] = useState(false);
    const [showDialog, setShowDialog] = useState(false);

    const height = '40%';


    const imageSource = trackInfo.album.images[trackInfo.album.images.length - 1].url;
    const largeImageSource = trackInfo.album.images[0].url;

    const options = [
        { label: 'Open on Spotify', onPress: () => openInSpotify(trackInfo.album.uri), icon: spotifyIcon },
        { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon},
        { label: 'Recommend', onPress: () => recommend(trackInfo, context), icon: recommendIcon},
        ]
    if (isDj) {
        options.push(
            { label: 'Queue', onPress: () => queueTrackFromSearch(trackInfo, context), icon: recommendIcon}
        )
    }
    if (trackInfo.preview_url) {
        options.push({ label: 'Preview', 
        onPress: () => {
            console.log("Pausing player...")
            context.pauseMusicPlayer();
            setShowDialog(true);
        },
        icon: heartIcon})
    }
    return (
        <View>
            <Card
                row
                style={styles.card}
                enableBlur
                useNative
                backgroundColor={Colors.white}
                activeOpacity={1}
                activeScale={true ? 0.96 : 1.04}
            >
                <Image
                    style={styles.image}
                    source={{uri: imageSource}}
                />
                <View margin-20 flex centerV>
                <TouchableOpacity onPress={openInSpotify}>
                    <Text text70 dark10>
                        {trackInfo.name}
                    </Text>
                    <Text text70 dark40>
                        {/* TODO include all the artists here */}
                        {trackInfo.artists[0].name}
                    </Text>
                </TouchableOpacity>
                </View>
                <View marginR-10 centerV flex right>
                    <TouchableOpacity onPress={() => setShowOptions(true)}>
                        <Entypo name="dots-three-vertical" size={18} color="black" />
                    </TouchableOpacity>
                </View>
            </Card>

            <ActionSheet
                renderTitle={() => {
                return (
                    <View marginT-20 centerH>
                    <Image
                        style={styles.image}
                        source={{uri: imageSource}}
                    />
                    <Text dark10 text70 >
                        {trackInfo.name}
                    </Text>
                    </View>
                );
                }}
                options={options}
                visible={showOptions}
                onDismiss={() => setShowOptions(false)}
            />
            <Dialog
                migrate
                useSafeArea
                height={height}
                panDirection={PanningProvider.Directions.UP}
                containerStyle={dialogStyle.roundedDialog}
                visible={showDialog}
                onDismiss={() => {
                    context.playMusicPlayer();
                    setShowDialog(false)
                }}
                pannableHeaderProps={{title: 'This is a pannable header Dialog'}}
                supportedOrientations={['portrait', 'landscape']}
            >
                <View centerV centerH>
                    <Image
                        style={{width: 300, height:300}}
                        source={{uri: largeImageSource}}
                    />
                </View>
                <PreviewTrack 
                    previewUri={trackInfo.preview_url} 
                    onFinish={()=> {
                        context.playMusicPlayer();
                        setShowDialog(false)
                    }}
                    posterUri={imageSource}
                />
            </Dialog>
        </View>
        
    )
}