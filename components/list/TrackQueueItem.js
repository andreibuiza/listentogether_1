import React from 'react';
import DefaultItem from './DefaultItem';
import {spotifyIcon, heartIcon} from './Styles';
import {openInSpotify} from './DefaultItemMethods';

export default function TrackQueueItem({ queueItem }) {
    const options = [
            { label: 'Open on Spotify', onPress: () => openInSpotify(queueItem.album.uri), icon: spotifyIcon },
            { label: 'Add to Liked Songs', onPress: () => spotifyLike(queueItem.id), icon: heartIcon},];
    return (
            <DefaultItem item={queueItem} options={options}/> 
      );
}
