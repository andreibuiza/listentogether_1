import React, {useState}from 'react';
import { TouchableOpacity } from 'react-native';
import styles, {spotifyIcon, heartIcon} from './Styles';
import {View, Text, ActionSheet, Card, Colors, Image} from 'react-native-ui-lib';
import { Entypo } from '@expo/vector-icons';
import {openInSpotify} from './DefaultItemMethods';


export default function DefaultItem({ item, options }) {
    const [showOptions, setShowOptions] = useState(false);


    return (
        <View marginT-10>
          <Card
              row
              style={styles.card}
              enableBlur
              useNative
              backgroundColor={Colors.white}
              activeOpacity={1}
              activeScale={true ? 0.96 : 1.04}
            >
              <Image
                  style={styles.image}
                  source={{uri: item.image}}
              />
              <View margin-20 flex centerV>
                <TouchableOpacity onPress={openInSpotify}>
                  <Text text70 dark10>
                      {item.name}
                  </Text>
                  <Text text70 dark40>
                      {/* TODO just the first artist for now */}
                      {item.artists[0].name}
                  </Text>
                </TouchableOpacity>
              </View>
              <View marginR-10 centerV flex right row>
                <View row marginR-20>
                  <Text marginR-20 style={styles.songStatus}>{item.score}</Text>
                </View>
                <TouchableOpacity onPress={() => setShowOptions(true)}>
                  <Entypo name="dots-three-vertical" size={18} color="black" />
                </TouchableOpacity>
              </View>
            </Card>
  
            <ActionSheet
              renderTitle={() => {
                return (
                  <View marginT-20 centerH>
                    <Image
                        style={styles.image}
                        source={{uri: item.image}}
                    />
                    <Text dark10 text70 >
                      {item.name}
                    </Text>
                  </View>
                );
              }}
              options={options}
              visible={showOptions}
              onDismiss={() => setShowOptions(false)}
            />
        </View>
      );
}
