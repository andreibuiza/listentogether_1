import React, { Component } from 'react';
import TrackHistoryItem from './TrackHistoryItem';
import AppContext from '../AppContext';
import {  View, FlatList, Text } from 'react-native';


class TrackHistoryList extends Component {
    context = this.context

    get trackHistory() {
        if (this.context.state.trackHistory) {
            return Object.keys(this.context.state.trackHistory).map(key => this.context.state.trackHistory[key])
        }
        return [];
    }

    render() {
        return(
            <View>
                <FlatList
                    data={Array.from(this.trackHistory)}
                    renderItem={({ item }) => 
                        <TrackHistoryItem trackHistoryItem={item}/>
                    }
                    keyExtractor={item => item.id}
                />
            </View> 
        )
    }
}

TrackHistoryList.contextType = AppContext;

export default TrackHistoryList;

