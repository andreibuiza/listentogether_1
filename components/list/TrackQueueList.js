import React, { Component } from 'react';

import AppContext from '../AppContext';
import DraggableFlatList from "react-native-draggable-flatlist";
import TrackQueueItemAdmin from "./TrackQueueItemAdmin";
import TrackQueueItem from "./TrackQueueItem";
import { View, FlatList } from 'react-native';
import db from '../../data/Database';


class TrackQueueList extends Component {
    context = this.context

    get trackQueue() {
        return this.context.state.trackQueue;
    }

    setNewQueueOrdering = (queue) => {
        db.setTrackQueue(queue, this.context.roomInfo.name);
    }

    render() {
        return(
            <View style={{ flex: 1 }}>
                { this.context.state.userInfo.isDj?
                    <DraggableFlatList
                        data={Array.from(this.trackQueue)}
                        renderItem={({ item, index, drag, isActive}) => 
                            <TrackQueueItemAdmin queueItem={item} index={index} drag={drag} isActive={isActive} context={this.context}/>
                            }
                        keyExtractor={item => item.id}
                        onDragEnd={({ data }) => this.setNewQueueOrdering(data)}
                    />
                    :
                    <FlatList
                        data={Array.from(this.trackQueue)}
                        renderItem={({item}) => 
                            <TrackQueueItem queueItem={item} />
                            }
                        keyExtractor={item => item.id}
                    />
                }
                
            </View> 
        )
    }
}

TrackQueueList.contextType = AppContext;

export default TrackQueueList;