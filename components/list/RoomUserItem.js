import React, {useState}from 'react';
import { TouchableOpacity } from 'react-native';
import styles, {spotifyIcon, heartIcon} from './Styles';
import {View, Text, ActionSheet, Card, Colors, Image} from 'react-native-ui-lib';
import { Entypo } from '@expo/vector-icons';
import {openInSpotify} from './DefaultItemMethods';


export default function RoomUserItem({ userItem }) {
const [showOptions, setShowOptions] = useState(false);

  const options = [
    { label: 'Open on Spotify', onPress: () => openInSpotify(userItem.userUri), icon: spotifyIcon } ] 

    return (
      <View marginT-10>
        <Card
            row
            style={styles.card}
            enableBlur
            useNative
            backgroundColor={Colors.white}
            activeOpacity={1}
            activeScale={true ? 0.96 : 1.04}
          >
            <Image
                style={styles.image}
                source={{uri: userItem.image}}
            />
            <View margin-20 flex centerV>
              <TouchableOpacity onPress={openInSpotify}>
                <Text text70 dark10>
                    {userItem.display_name}
                </Text>
              </TouchableOpacity>
            </View>
            <View marginR-10 centerV flex right row>
              <TouchableOpacity onPress={() => setShowOptions(true)}>
                <Entypo name="dots-three-vertical" size={18} color="black" />
              </TouchableOpacity>
            </View>
          </Card>

          <ActionSheet
            renderTitle={() => {
              return (
                <View marginT-20 centerH>
                  <Image
                      style={styles.image}
                      source={{uri: userItem.image}}
                  />
                  <Text dark10 text70 >
                    {userItem.display_name}
                  </Text>
                </View>
              );
            }}
            options={options}
            visible={showOptions}
            onDismiss={() => setShowOptions(false)}
          />
      </View>

    );
}
