import musicClient from '../Spotify';
import { Linking} from "react-native";
import db from '../../data/Database';
import RoomTrack from '../../data/structure/RoomTrack';


// Available actions
export const openInSpotify = async (uri) => {
    let spotifyUrl = uri;
    console.log("opening: " + spotifyUrl);
    const spotifyInstalled = await Linking.canOpenURL(spotifyUrl);
    if (spotifyInstalled) {
        await Linking.openURL(spotifyUrl);
    } else {
        // TODO display a toast if spotify not installed.
    }
}
export const spotifyLike = (id) => {
    musicClient.addTrackToLikedSongs(id);
}

export const recommend = (trackToRecommend, context) => {
    let recommendInfo = new RoomTrack(trackToRecommend);
    db.saveSongRecommendation(recommendInfo.get(), context.roomInfo.name);
}

export const queueTrackFromSearch = (trackToQueue, context) => {
    let trackInfo = new RoomTrack(trackToQueue);
    queueTrack(trackInfo.get(), context);
}

export const queueTrack = (roomTrack, context) => {
    let newQueue = context.state.trackQueue;
    newQueue.push(roomTrack)
    db.setTrackQueue(newQueue, context.roomInfo.name);
}
export const deleteTrackFromQueue = (index, context) => {
    let newQueue = context.state.trackQueue;
    newQueue.splice(index, 1);
    db.setTrackQueue(newQueue, context.roomInfo.name);
}

const onBuffer = () => {console.log("preview buffering")}
const videoError = (error) => {
    console.log("Error loading the track preview: ");
    console.log(error);
}
import React, {useState} from 'react';
import Video from 'react-native-video';
export function PreviewTrack ({previewUri, posterUri, onFinish}) {
    const videoRef = React.createRef();
    return(
        // TODO: figure out what ref is for, I'm currently getting lots of warnings for it
        // TODO: setup the view while preview (image, a 30 second timer with the moving dot)
        // TODO: apply the preview for all tracks (history, recommend)

        <Video source={{uri: previewUri}}   
            ref={ref =>{}}              
            onBuffer={onBuffer}                
            onError={videoError}  
            audioOnly={true} 
            poster={posterUri}
            onEnd={() => {
                    onFinish();
                }
            }
        />
    )
}