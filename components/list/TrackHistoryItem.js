import React, {useState}from 'react';
import { TouchableOpacity } from 'react-native';
import styles, {spotifyIcon, heartIcon} from './Styles';
import {View, Text, ActionSheet, Card, Colors, Image} from 'react-native-ui-lib';
import { Entypo } from '@expo/vector-icons';
import {openInSpotify, spotifyLike} from './DefaultItemMethods';
import DefaultItem from './DefaultItem';


export default function TrackHistoryItem({ trackHistoryItem }) {
  const trackInfo = trackHistoryItem;

  const options = [
    { label: 'Open on Spotify', onPress: () => openInSpotify(trackHistoryItem.album.uri), icon: spotifyIcon },
    { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon}, ]

    return (
      <DefaultItem item={trackHistoryItem} options={options}/> 
    );
}
