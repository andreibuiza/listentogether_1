import React, { Component } from 'react';
import AppContext from '../AppContext';
import RecommendItem from './RecommendItem';  
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import update from 'immutability-helper';


class RecommendList extends Component {
    context = this.context

    state = {
        // TODO is it ok to have recommendations list as a state and not saved in app context?
        recommendations: new Map(),
    };  

    componentDidMount() {
        db.listenToRecommendationChanges(
            (r) => this.updateRecommendationState(r, false),
            this.context.roomInfo.name
          );
        db.listenToRecommendationDelete(
            (r) => this.updateRecommendationState(r, true),
            this.context.roomInfo.name
          );
    }

    updateRecommendationState = (r, isDelete) => {
        const newState = (isDelete)? update(this.state, 
            {
                recommendations: {$remove: [r.key]},
            }) 
            : update(this.state, 
            {
                recommendations: {$add: [[r.key, {recommendation: r.recommendation, key: r.key}]]},
            });
        this.setState(newState);
    }

    updateRecommendation = (room) => {
        return (updatedRecommendation, id) => {
            db.updateRecommendation(updatedRecommendation, room, id);
        }
    }

    isDj = () => {
        return this.context.state.userInfo.isDj;
    }


    render() {
        const onPress = {
                update : this.updateRecommendation(this.context.roomInfo.name),
        }
        return(
            <View>
                <FlatList
                    data={Array.from(this.state.recommendations.values())}
                    renderItem={({ item }) => 
                        <RecommendItem item={item}    
                        onPress = {
                            onPress
                        }
                        isDj = {this.isDj()}
                        context = {this.context} />
                    }
                    keyExtractor={item => item.id}
                    extraData={this.state}
                />
            </View> 
        )
    }
}
RecommendList.contextType = AppContext;
   
export default RecommendList;
