import React, { Component } from 'react';
import MusicPlayer from './musicPlayer/MusicPlayer'
import RecommendList from './list/RecommendList';
import SearchMusic from './SearchMusic';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {  StyleSheet, TouchableOpacity } from 'react-native';
import {
  Colors,
  Button,
  Constants,
  ActionSheet,
  View,
  Image,
  Text
} from 'react-native-ui-lib';
import { Icon } from 'native-base';
import { Feather, MaterialIcons } from '@expo/vector-icons';
import Chat from './Chat';
import AppContext from './AppContext';
import {openInSpotify} from './list/DefaultItemMethods';
import {spotifyIcon} from './list/Styles';



const Tab = createBottomTabNavigator();

class MusicLayout extends Component {
  context = this.context

  state = { 
    showDialog: false,
  }

  hideDialog = () => {
    this.setState({showDialog: false});
  };

  showDialog = () => {
    this.setState({showDialog: true});
  };

  options = () => {
    return (
      <TouchableOpacity
        onPress = {this.showDialog}
      >
        <Icon type="Entypo" name="dots-three-vertical" />
      </TouchableOpacity>
    );
  }

  navigateOut = () => {
    this.props.navigation.navigate("Main")
  }

  renderOptionsScreen = () => {
    return (
      <Button text60
        label = {"Exit"}
        link
        onPress = {this.navigateOut}
      />
    );
  }

  componentWillUnmount() {
    this.context.leaveRoom();
  }

  goToUserList = () => {
    this.props.navigation.navigate('RoomUserList');
  }


  renderRoomOptionsDialog = () => {
    const {showDialog} = this.state;
    const height = '20%';
    const userIcon = require('../assets/usersIcon.png')
    const closeIcon = require('../assets/closeIcon.png')
    const roomImage = this.context.state.trackImageUri;
    const options = [
      { label: 'Open on Spotify', onPress: () => openInSpotify(this.context.state.playerState.album.uri), icon: spotifyIcon },
      { label: 'Who\'s here?', onPress: this.goToUserList, icon: userIcon },
      { label: 'Exit', onPress: this.navigateOut, icon: closeIcon}, ]; 
    return (

      <ActionSheet
      renderTitle={() => {
        return (
          <View marginT-20 centerH>
            <Image
                style={{
                  width: 80,
                  height: 80
                }
                }
                source={{uri: roomImage}}
            />
            <Text dark10 text70 >
              {this.context.state.roomInfo.name}
            </Text>
          </View>
        );
      }}
      options={options}
      visible={showDialog}
      onDismiss={this.hideDialog}
      />
    );
  };

  render() {
    this.props.navigation.setOptions({
      headerRight: () => (this.options()),
      headerLeft: null
    });
    return (
      <>
        <Tab.Navigator
        initialRouteName="Feed"
        tabBarOptions={{
          activeTintColor: '#e91e63',
        }}
      >
        <Tab.Screen
          name="Chat"
          component={Chat}
          options={{
            tabBarLabel: 'Chat',
            tabBarIcon: ({ color, size }) => (
              <MaterialIcons name="chat-bubble-outline" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="MusicPlayer"
          component={MusicPlayer}
          options={{
            tabBarLabel: 'Now Playing',
            tabBarIcon: ({ color, size }) => (
              <Feather name="music" size={size} color={color}/>
            ),
          }}
        />
        <Tab.Screen
          name="Community Recommendations"
          component={RecommendList}
          options={{
            tabBarLabel: 'Recommendations',
            tabBarIcon: ({ color, size }) => (
              <Feather name="list" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Search"
          component={SearchMusic}
          options={{
            tabBarLabel: 'Search',
            tabBarIcon: ({ color, size }) => (
              <Feather name="search" size={size} color={color} />
            ),
          }}
        />
      </Tab.Navigator>
      {this.renderRoomOptionsDialog()}
      
      </>
    );
  }
}

MusicLayout.contextType = AppContext;
export default MusicLayout;

const dialogStyle = StyleSheet.create({
  dialog: {
    backgroundColor: Colors.white
  },
  roundedDialog: {
    backgroundColor: Colors.white,
    marginBottom: Constants.isIphoneX ? 0 : 20,
    borderRadius: 12
  },
  button: {
    margin: 5,
    alignSelf: 'flex-start'
  },
  verticalScroll: {
    marginTop: 20
  },
  horizontalTextContainer: {
    alignSelf: 'center',
    position: 'absolute',
    top: 10
  }
});