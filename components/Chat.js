import React, { Component } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import db from '../data/Database.js';
import AppContext from './AppContext';
import styles from './Styles';


const recommendRegex = "#recommend https:\/\/open\.spotify\.com\/track\/(\w+)";

class Chat extends Component {
  context = this.context
  state = {
    messages: [],
  };

  listenTogetherUser = {
    name: "ListenTogther",
    _id: "ListenTogther"
  }

  get roomName() {
    return this.context.roomInfo.name;
  }

  get user() {
    return { 
      _id: this.context.state.userInfo.id,
      name: this.context.state.userInfo.display_name,
      avatar: this.context.state.userInfo.images[0].url
    };
  }

  get listenButtonTitle() {
    if (this.user.name == "admin") {
      return "Play Music";
    } else {
      return "Listen Now";
    } 
  }

  parseMessage = (messages) => {
    db.onSend(messages, this.roomName)
  }

  addUnsavedMessage = (msg) => {
    const timestamp = new Date();
    const message = {
      _id: 1,
      timestamp,
      text: msg,
      system: true
    };
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, message),
    }))
  }

  render() {
    
    return (
      <GiftedChat
          messages={this.state.messages}
          onSend={this.parseMessage}
          user={this.user}
          parsePatterns={(linkStyle) => [
            { pattern: /#(\w+)/, style: styles.hashTag},
          ]} 
      />
    );
  };

  componentDidMount() {
    if (this.context.state.userInfo.isDj) {
      this.addUnsavedMessage("Welcome to " + this.roomName + "!!!");
    }
    db.getMessages(
      message => {
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, message),
        }))
      },
      this.roomName
    );
  }
  componentWillUnmount() {
    db.disconnectRoom(this.roomName);
  }
} 
Chat.contextType = AppContext;

export default Chat;
