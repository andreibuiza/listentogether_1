import React, { Component } from 'react';
import AppContext from './AppContext';
import styles from './Styles.js';
import { View, TextInput } from 'react-native';
import { Form, Item, Label, Button, Icon, Text } from 'native-base';
import db from '../data/Database.js';
import RoomUserInfo from '../data/structure/RoomUserInfo.js';

import {
    Toast, Colors
  } from 'react-native-ui-lib';
const DEFAULT_ROOM_SIZE = 10
class CreateRoom extends Component {
    context = this.context
    state = { 
        name: '',
        maxNumAudience: DEFAULT_ROOM_SIZE,
        showTopToast: false,
        toastMessage: ""
    }
    onChangeName = name => this.setState({ name: name })

    onChangeMaxOccupants = max => this.setState({ maxNumAudience: max })

    dismissTopToast = () => {
        this.setState({showTopToast: false});
    }

    showTopToast = (message) => {
        this.setState({showTopToast: true, toastMessage: message});
    }

    getRoomCallback = existingRoomInfo => {
        if(existingRoomInfo) {
            this.showTopToast("Room already exists");
        } else {
            let updatedUserState = this.context.state.userInfo;
            updatedUserState.isDj = true
            this.context.updateUserState(updatedUserState)
            let basicUser = new RoomUserInfo(updatedUserState);

            let roomInfo = {
                name : this.state.name,
                dj : {
                    name: this.context.state.userInfo.display_name,
                    id: this.context.state.userInfo.id
                },
                maxNumAudience: this.state.maxNumAudience,
                score: 0
            }

            db.createRoom(roomInfo, this.state.name)
            db.addUserToRoom(roomInfo.name, basicUser.get());
            
            
            this.context.initializeRoomInfo(roomInfo);
            
            this.props.navigation.navigate('MusicLayout', { state: {roomName: this.state.name }});
        }
    };

    onPressCreate = () => {
        db.getRoomInfo(this.getRoomCallback, this.state.name)
    }

    render() {
        return (
          <View>
            <Form>
                <Item stackedLabel>
                    <Label>Room name</Label>
                    <TextInput onChangeText={this.onChangeName}/>
                </Item>
                <Item stackedLabel last>
                    <Label>Room size</Label>
                    <TextInput 
                        keyboardType = 'numeric'
                        defaultValue = '10'
                        onChangeText={this.onChangeMaxOccupants}/>
                </Item>
            </Form>
            <Button large rounded iconLeft dark
                style={styles.genericButton}
                onPress={this.onPressCreate}>
                <Icon name='add-circle' />
                <Text>Create</Text>
            </Button>

            <Toast
                visible={this.state.showTopToast}
                position={'Top'}
                message={this.state.toastMessage}
                onDismiss={this.dismissTopToast}
                showDismiss={true}
                backgroundColor = {Colors.red30}
            />
          </View>


          );
      }
}

CreateRoom.contextType = AppContext;
export default CreateRoom;