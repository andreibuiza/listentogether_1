import React, { Component } from 'react';
import AppContext from './AppContext';
import SearchBar from 'react-native-search-bar';
import musicClient from './Spotify';
import {  FlatList } from 'react-native';
import TrackSearchResultItem from './list/TrackSearchResultItem';
import { FontAwesome5 } from '@expo/vector-icons'; 
import {View, Text, Card, Colors} from 'react-native-ui-lib';
import styles from './list/Styles';

class SearchMusic extends Component {
    context = this.context

    state = { 
        searchResults: null,
    }
    updateSearchResult = async (text) => {
        if (text == "" || text.charAt(0) == ' ') {
            this.setState((oldState) => (
                {...oldState, searchResults: null}));
            return;
        }
        let q = text;
        let results = await musicClient.searchSpotifyEntity(q);
        this.setState((oldState) => (
            {...oldState, searchResults: results}));
    }

    cancelButtonPressed = () => {
        console.log("cancel button pressed")
    }

    onPressHistoryTile = () => {
        this.props.navigation.navigate('TrackHistoryList');
    }

    onPressQueueTile = () => {
        this.props.navigation.navigate('TrackQueueList');
    }

    render() {
        const searchBar = React.createRef();
        return(
        <View>
            {/* <SpotifyEntityListItem context = {this.context}/> */}
            {/* <TrackHistoryList/> */}
            <SearchBar
                ref={searchBar}
                placeholder="Search"
                onChangeText={(text) => this.updateSearchResult(text)}
                onSearchButtonPress={(text) => this.updateSearchResult(text)}
                onCancelButtonPress={() => this.cancelButtonPressed()}
                onSearchButtonPress={searchBar.unFocus}
            />
            {/* TODO: add a tile for Room Track History List */}
            {/* TODO: display search results */}
            {this.state.searchResults?
                <>
                <View marginT-20>
                    <FlatList
                        data={Array.from(this.state.searchResults.tracks.items)}
                        renderItem={({ item }) => 
                            <TrackSearchResultItem trackInfo={item} context={this.context} isDj={this.context.state.userInfo.isDj}/>
                        }
                        keyExtractor={item => item.id}
                    />
                </View>
                </>
                :
                <>
                <Card marginT-20
                    row
                    style={styles.card}
                    enableBlur
                    useNative
                    backgroundColor={Colors.white}
                    activeOpacity={1}
                    activeScale={true ? 0.96 : 1.04}
                    onPress={()=> this.onPressHistoryTile()}
                >
                    <View marginL-20 flex centerV row>
                        <FontAwesome5 name="history" size={24} color="black" />
                        <Text marginL-20 text70 dark10>
                            Track History
                        </Text>
                    </View>
                </Card>
                <Card marginT-20
                    row
                    style={styles.card}
                    enableBlur
                    useNative
                    backgroundColor={Colors.white}
                    activeOpacity={1}
                    activeScale={true ? 0.96 : 1.04}
                    onPress={()=> this.onPressQueueTile()}
                >
                    <View marginL-20 flex centerV row>
                        <FontAwesome5 name="history" size={24} color="black" />
                        <Text marginL-20 text70 dark10>
                            Queue
                        </Text>
                    </View>
                </Card>
                </>

            }
            
        </View>
        )
    }
}
SearchMusic.contextType = AppContext;

export default SearchMusic;
