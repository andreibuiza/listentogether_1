import * as firebase from 'firebase';
const firebase2 = require("firebase");
// Required for side-effects
require("firebase/functions");

class Fire {
    constructor(dbPath) {
        this.dbPath = dbPath;
        this.init();
        this.observeAuth();
    }

    init = () =>  firebase.initializeApp({
        apiKey: "AIzaSyAjyMF2wH23UjIEQniFcDy1_DuriNFaMRk",
        authDomain: "basicchat-21c35.firebaseapp.com",
        databaseURL: "https://basicchat-21c35.firebaseio.com",
        projectId: "basicchat-21c35",
        storageBucket: "basicchat-21c35.appspot.com",
        messagingSenderId: "14737914344",
        appId: "1:14737914344:web:b1f2297fcfc8a707634c5d",
        measurementId: "G-Z25DN4BDX5"
    });

    observeAuth = () => firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

    skipSong = (roomName) => {
        let skipSongFunction = firebase2.functions().httpsCallable('skipSong');
        skipSongFunction({room: roomName});
    }

    // TODO who is the user? how to make one?
    onAuthStateChanged = user => {
        if (!user) {
          try {
            firebase.auth().signInAnonymously().catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            }); 
          } catch ({ message }) {
            alert(message);
          }
        }
        console.log("Logged in");
    }
    getRef(path) {
        // This connects to Firebase real time database
        return firebase.database().ref(path);
    }

    on = (callback, path, parse) => this.getRef(path)
      .limitToLast(20)
      .on('child_added', snapshot => callback(parse(snapshot)));

    onOnce = (callback, path, parse) => this.getRef(path)
        .once('value', snapshot => {
            callback(parse(snapshot))
        }
    );

    onChildChanged = (callback, path, parse) => this.getRef(path)
        .on('child_changed', snapshot => callback(parse(snapshot)));
    
    onChildDelete = (callback, path, parse) => this.getRef(path)
        .on('child_removed', snapshot => callback(parse(snapshot)));
    
    subscribeToData = (callback, path, parse) => this.getRef(path)
        .on('value', data => {
            callback(parse(data))
        });

    delete = (path) => this.getRef(path).remove();

    off(path) {
        this.getRef(path).off();
    }
    get uid() {
        return (firebase.auth().currentUser || {}).uid;
    }
    get timestamp() {
        return firebase.database.ServerValue.TIMESTAMP;
    }
   
    append = (data, path) => this.getRef(path).push(data);

    set = (data, path) => this.getRef(path).set(data);

    // TODO add logic to grab the specifed track id
}

// Instead of init here, I must make init this
// Fire.shared = new Fire();
export default Fire;