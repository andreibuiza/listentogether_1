// TODO think about a pattern for re-usable interfaces, using different data sources
// TODO how are classes imported in a file,s and how can I instantiate an object 
import Fire from './source/Fire.js';


/*
    When getting data from this DB, caller should provide the callback once data is pulled. 
*/
class Database {
    constructor() {
        this.fire = new Fire();
    }

    getMessages(callback, room) {
        path = 'messages/' + room;
        this.fire.on(callback, path, this.parseMessageSnapshot);
    }


    // SAMPLE
        /*
        let messages = [
            {
                _id: 1,
                text: 'My message',
                createdAt: new Date(Date.UTC(2016, 5, 11, 17, 20, 0)),
                user: {
                  _id: 2,
                  name: 'React Native',
                  avatar: "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10152514067013072&height=300&width=300&ext=1600972338&hash=AeSo-i5NezzAixBt",
                },
                image: "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10152514067013072&height=300&width=300&ext=1600972338&hash=AeSo-i5NezzAixBt",
                // Mark the message as sent, using one tick
                sent: true,
                // Mark the message as received, using two tick
                received: true,
                // Mark the message as pending with a clock loader
                pending: true,
                // Any additional custom parameters are passed through
              },
              {
                _id: 2,
                text: 'YES',
                createdAt: new Date(Date.UTC(2016, 5, 11, 17, 20, 0)),
                user: {
                  _id: 2,
                  name: 'React Native',
                  avatar: "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10152514067013072&height=300&width=300&ext=1600972338&hash=AeSo-i5NezzAixBt",
                },
              }
        ]
        */
    parseMessageSnapshot = snapshot => {
        const { timestamp: numberStamp, text, user } = snapshot.val();
        const { key: _id } = snapshot;
        const timestamp = new Date(numberStamp);
        const message = {
            _id,
            timestamp,
            text,
            user,
        };
        return message;
    }


    disconnectRoom(room) {
        path = 'messages/' + room;
        this.fire.off(path);
    }

    onSend(messages, room){
        path = 'messages/' + room;
        for (let i = 0; i < messages.length; i++) {
            const { text, user } = messages[i];
            const message = {
                text,
                user,
                timestamp: this.fire.timestamp,
            };
            this.fire.append(message, path);
        }
    }   

    onUpdateMusic(musicInfo, room) {
        path = 'music/' + room;
        console.log(musicInfo)
        return this.fire.set(musicInfo, path);
    }

     
    deleteRoomMusic(room) {
        path = 'music/' + room;
        this.fire.delete(path);
    }

    createRoom(roomInfo, room) {
        path = 'roomMeta/' + room;
        console.log("creating new room");
        console.log(roomInfo)
        this.fire.set(roomInfo, path);
    }
   

    /*
    change /users and /history to be its own thing
    */
    addUserToRoom(room, user) {
        path = 'roomMeta/' + room + '/users/' + user.id;
        this.fire.set(user, path);
    }

    removeUserFromRoom(room, user) {
        path = 'roomMeta/' + room + '/users/' + user.id;
        console.log("removing user:" + user)
        this.fire.delete(path);
    }

    subscribeToRoomInfo(callback, room) {
        this.fire.subscribeToData(callback, this.roomMetaPath(room), (data) => {
            return data.val()});
    }

    subscribeToRoomTrackHistory(callback, room) {
        this.fire.subscribeToData(callback, this.roomTrackHistoryPath(room), (data) => {
            return data.val()});
    }

    getRoomInfo(callback, room) {
        path = 'roomMeta/' + room;
        return this.fire.onOnce(callback, path, (data) => {
            return data.val()})
    }

    updateMusicScore(value, room) {
        path = 'roomMeta/' + room + '/score';
        this.fire.set(value, path);
    }

    disconnectRoomInfoSub(room) {
        path = 'roomMeta/' + room;
        this.fire.off(path);
    }

    getCurrentSong(callback, room) {
        path = 'music/' + room;
        console.log("getting music from room:" + room);
        this.fire.subscribeToData(callback, path, this.parseMusic);
    }

    disconnectSong(room) {
        path = 'music/' + room;
        this.fire.off(path);
    }

    parseMusic = data => {
        return data.val();
    }

    get uid() {
        return this.fire.uid;
    }
    
    // TODO throw an exception if something goes wrong
    saveSongRecommendation(recommendationItem, room) {
        path = 'recommend/' + room + '/' + recommendationItem.id;
        // TODO check that recommended song is unique
        this.fire.set(recommendationItem, path)
    }

    listenToRecommendationChanges(callback, room) {
        path = 'recommend/' + room
        this.fire.on(callback, path, this.parseRecommend);
        this.fire.onChildChanged(callback, path, this.parseRecommend);
    }

    listenToRecommendationDelete(callback, room) {
        path = 'recommend/' + room
        this.fire.onChildDelete(callback, path, this.parseRecommend)
    }

    updateRecommendation(recommendation, room, id) {
        path = 'recommend/' + room + '/' + id
        this.fire.set(recommendation, path);
    }

    
    deleteRecommendation(room, id) {
        path = 'recommend/' + room + '/' + id
        this.fire.delete(path);
    }

    disconnectRecommendation(room) {
        path = 'recommend/' + room;
        this.fire.off(path);
    }

    parseRecommend = data => {
        return {recommendation: data.val(), key: data.val().id};
    }

    getSongRecommendation(id, room) {
        path = 'recommend/' + room
        // TODO try to get a recommendation with the same id
        // in Chat layer I should update the recommendation to +1 vote when same 
        // song is recommended
    }

    getQueue(callback, room) {
        this.fire.onOnce(callback, this.roomTrackQueuePath(room), data => data.val())
    }
    
    setTrackQueue(trackQueueList, room) {
        this.fire.set(trackQueueList, this.roomTrackQueuePath(room));
    }

    parseQueue = data => {
        return data.toJSON();
    }
    subscribeToRoomTrackQueue(callback, room) {
        this.fire.on(callback, this.roomTrackQueuePath(room), this.parseQueue);
        this.fire.onChildChanged(callback, this.roomTrackQueuePath(room), this.parseQueue);
        this.fire.onChildDelete(callback, this.roomTrackQueuePath(room), this.parseQueue)
    }

    roomMetaPath = (roomName) => 'roomMeta/' + roomName;
    roomTrackQueuePath = (roomName) => 'queue/' + roomName;
    roomTrackHistoryPath = (roomName) => 'history/' + roomName;
/*
 Decide between using append or set for updating the queue. Definitely order must be respected, ease of item ordering, 
 Figure out the component design for enabling touch order manage of the queue list
*/
};
db = new Database();
export default db;



