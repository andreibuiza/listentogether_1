export default class RoomTrack {
    roomTrack = {};
    constructor (trackInfo) {
        const re = /spotify:track:(\w+)/;
        let id = re.exec(trackInfo.uri)[1];
        this.roomTrack = {
            album: {
                name: trackInfo.album.name,
                uri: trackInfo.album.uri
            },
            artists: trackInfo.artists,
            duration: trackInfo.duration_ms,
            name: trackInfo.name,
            uri: trackInfo.uri,
            name: trackInfo.name,
            id,
            score: 0,
            image: trackInfo.album.images[1].url
        }
    }

    get = () => {
        return this.roomTrack;
    }
}