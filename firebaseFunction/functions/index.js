const functions = require('firebase-functions');

const admin = require('firebase-admin');
let app = admin.initializeApp();
let timerIdMap = new Map();
var database = admin.database(app);
functions.logger.log("Initialized database. This is the root: " + database.ref());
exports.updateRoomTrackInfo = functions.database.instance('basicchat-21c35').ref('/queue/{roomName}').onCreate((snap, context) => {
    functions.logger.log('New room: ' + context.params.roomName + " listener activated", context.params.roomName);
    return updateRoomTrack(context.params.roomName, false);
})

exports.skipSong = functions.https.onCall((data, context) => {
    let roomName = data.room;
    functions.logger.log('Skip song called, room: ' + roomName);
    deleteTimerOfRoom(roomName);
    updateRoomTrack(roomName, true);
});

deleteTimerOfRoom = (roomName) => {
    clearTimeout(timerIdMap.get(roomName));
    timerIdMap.delete(roomName);
}

waitForTrackToFinish = (duration, onFinish, roomName) => {
    let timerId = setTimeout(() => {
        onFinish();
    }, duration);
    timerIdMap.set(roomName, timerId)
}

updateRoomTrack = async (roomName, isSkip) => {
    functions.logger.log('*********updateRoomTrack************');

    let trackQueue = await getData(queuePath(roomName));
    
    let roomTrack = await getData(roomTrackPath(roomName));

    if (roomTrack) {
        let duration = roomTrack.duration;
        let currentTime = Date.now();
        let trackIsDone = ((roomTrack.timeTrackStarted + duration) <  currentTime)? true: false;
        functions.logger.log('*********room track status************');
        functions.logger.log('trackIsDone: ' + trackIsDone);
        functions.logger.log('Track ' + roomTrack.name + ' is at: ' + (currentTime - roomTrack.timeTrackStarted) + ' s');
        if (!isSkip && !trackIsDone) {
            return;
        }
        if (isSkip) {
            functions.logger.log('*********track skipped************');
            deleteTimerOfRoom(roomName)
        } else {
            functions.logger.log('*********track finished************');
        }
        // TODO this should happen also when the queue is empty.
        await addTrackToHistoryList(roomTrack, roomName);
        // Delete the current track
        await deleteCurrentTrack(roomName);
    }
    
    if (!trackQueue) {
        return;
    }

    let newTrack = trackQueue.shift();
    functions.logger.log('*********loading new track************');


    // Use transaction when shifting queue https://firebase.google.com/docs/database/web/read-and-write#save_data_as_transactions   
    // if queue is empty, delete /queue/roomName
    if (trackQueue.length === 0){
        await deleteP(queuePath(roomName));
    }

    await set(trackQueue, queuePath(roomName));
    let buffer = 0
    waitForTrackToFinish(newTrack.duration + buffer, () => updateRoomTrack(roomName), roomName);
    return await setTrackAsRoomTrack(newTrack, roomName);
}



setTrackAsRoomTrack = (trackInfo, roomName) => {
    trackInfo.timeTrackStarted = Date.now();
    trackInfo.isRoomTrackPaused = false;
    set(trackInfo.score, roomScorePath(roomName));
    return set(trackInfo, roomTrackPath(roomName));
}

addTrackToHistoryList = async (trackInfo, roomName) => {
    trackInfo.score = await getData(roomScorePath(roomName));
    functions.logger.log('*********adding song to History List************');
    functions.logger.log(trackInfo);
    push(trackInfo, historyPath(roomName)).catch(error => functions.logger.log(error));
}

deleteCurrentTrack = (roomName) => {
    functions.logger.log('*********Deleting Track************');
    deleteP(roomTrackPath(roomName));
}

// TODO we can call an endpoint here to update the Track to next inQueuef

const roomTrackPath = (roomName) => '/music/' + roomName;
const roomMetaPath = (roomName) => '/roomMeta/' + roomName;
const roomScorePath = (roomName) => roomMetaPath(roomName) + '/score';

const queuePath = (roomName) => '/queue/' + roomName;
const historyPath = (roomName) => '/history/'+ roomName;

const set = (data, path) => database.ref(path).set(data);
const push = (data, path) => database.ref(path).push(data);

const deleteP = (path) => database.ref(path).remove();

const get = (path) => database.ref(path).once('value');

const getData = async (path) => {
    let data = await get(path);
    return data.val();
}