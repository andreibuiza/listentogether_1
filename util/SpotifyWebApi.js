import RestClient from 'react-native-rest-client';
 
export default class SpotifyWebApi extends RestClient {
    constructor(authToken) {
        super("https://api.spotify.com/v1", {
          headers: {
            // Include as many custom headers as you need
            Authorization: `Bearer ${authToken}`
          },
          // Simulate a slow connection on development by adding
          // a 2 second delay before each request.
        //   devMode: __DEV_,
        //   simulatedDelay: 2000
        });
    }
    getTrack(id) {
      return this.GET('/tracks/' + id );
    }

    getUser() {
      return this.GET('/me');
    }

    addTrackToYourMusic(id) {
      this.PUT('/me/tracks?ids=' + id);
    }

    searchSpotifyEntity(q) {
      // search only track for now 
      return this.GET('/search?q=' + encodeURIComponent(q) + '&type=track&limit=10');
    }


    startSongAtPosition(uri, position) {
      this.PUT('/me/player/play', 
      {
        "uris": [uri],
        "position_ms": position
      })
    }
};